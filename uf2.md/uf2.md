## 10 novembre 2020

##lsblk
- La comanda lsblk serveix per a extraure informacio de les particions, sistemes de fitxers i punts de muntatge:

[guest@a06 ~]$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  17.9M  1 loop /var/lib/snapd/snap/pdftk/9
loop1    7:1    0  97.1M  1 loop /var/lib/snapd/snap/core/9993
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]

##-O, --output-all     output all columns
- Ens dona tota la informacio del disc.

WSAME WWN                RAND PKNAME HCTL       TRAN   SUBSYSTEMS  REV VENDOR ZONED
loop0
     loop0 /dev/loop0
                  7:0         0  17.9M squash  17.9M   100% 4.0   /var/lib/s                                                                                                                                                                       128  1  0       0               17.9M       root  disk  brw-rw----         0    512      0     512     512    1 mq-deadline
                                                                                                                                                                                                                                                                                                                                                                             256 loop        0        4K       4G         0    0B                       0                          block                  none
loop1
     loop1 /dev/loop1
                  7:1         0  97.1M squash  97.1M   100% 4.0   /var/lib/s                                                                                                                                                                       128  1  0       0               97.1M       root  disk  brw-rw----         0    512      0     512     512    1 mq-deadline


##lsblk -o NAME,MODEL
- Aquesta comanda et dona la informacio que buscas posant al costat de la comanda el que vols trobar.
NAME   MODEL
loop0  
loop1  
sda    ST500DM002-1BD142
├─sda1 
├─sda5 
├─sda6 
└─sda7 

##Alias
- Serveix per a crear comandes per a que mes envdevant sigui mes facil posarla i que la recordis.
[root@a06 ~]# alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT'
[root@a06 ~]# lsdisk
NAME   MODEL             FSTYPE     SIZE TYPE MOUNTPOINT
loop0                    squashfs  17.9M loop /var/lib/snapd/snap/pdftk/9
loop1                    squashfs  97.1M loop /var/lib/snapd/snap/core/9993
sda    ST500DM002-1BD142          465.8G disk 
├─sda1                                1K part 
├─sda5                   ext4       100G part /
├─sda6                   ext4       100G part 
└─sda7                   swap         5G part [SWAP]

##SMART
- Monitoriza i reporta informacio sobre el disc dur, hi ha una comanda que es el smertctl -a.
================================================== SMARTCTL EXAMPLES =====

  smartctl --all /dev/sda                    (Prints all SMART information)

  smartctl --smart=on --offlineauto=on --saveauto=on /dev/sda
                                              (Enables SMART on first disk)

  smartctl --test=long /dev/sda          (Executes extended disk self-test)

  smartctl --attributes --log=selftest --quietmode=errorsonly /dev/sda
                                      (Prints Self-Test & Attribute errors)
  smartctl --all --device=3ware,2 /dev/sda
  smartctl --all --device=3ware,2 /dev/twe0
  smartctl --all --device=3ware,2 /dev/twa0
  smartctl --all --device=3ware,2 /dev/twl0
          (Prints all SMART info for 3rd ATA disk on 3ware RAID controller)
  smartctl --all --device=hpt,1/1/3 /dev/sda
          (Prints all SMART info for the SATA disk attached to the 3rd PMPort
           of the 1st channel on the 1st HighPoint RAID controller)
  smartctl --all --device=areca,3/1 /dev/sg2
          (Prints all SMART info for 3rd ATA disk of the 1st enclosure
           on Areca RAID controller)


##Hi ha 2 tipus de tecnologia:

- Disc Rotacional
El el disc dur habitual que te plats magnetics i un braç de lectura 

- Disc SSD 
Son chips que alacenen arxius en microchips amb memoria flash interconectades entre si.


##df -h
- Et dona especificacions del disc com capacitat,memoria utilizada.

##dd if=/dev/urandom of=/mnt/prova2G bs=2048 count=1048576
- Aquesta comanda crea un fitxer.


**16 de novembre 2020**

#Extracio informacio sobre hardware

#sistema

model de sistema, versió de bios i data de bios, quina edat té el maquinari?
-dmidecode -s system-product-name
        -H81M-S2PV
-dmidecode -t bios
        -Vendor: American Megatrends Inc.
	-Version: F5
	-Release Date: 01/17/2014
	-Address: 0xF0000 
	-Runtime Size: 64 kB
	-ROM Size: 8 MB
 -La maquinaria te 6 anys.     
model de placa base, enllaç al manual, enllaç al producte
-dmidecode -t baseboard
        -Product Name: H81M-S2PV
        -Enllaç manual: https://download.gigabyte.com/FileList/Manual/mb_manual_ga-h81m-s2pv_e.pdf
        -Enllaç producte: http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#kf

bancs de memòria (lliures o ocupats)
-lshw -class memory
        - description: System Memory
       physical id: 7
       slot: System board or motherboard
       size: 8GiB
     *-bank:0
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 0
          serial: [Empty]
          slot: ChannelA-DIMM0
     *-bank:1
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 1
          serial: [Empty]
          slot: ChannelA-DIMM1
     *-bank:2
          description: DIMM DDR3 Synchronous 1400 MHz (0.7 ns)
          product: 99U5471-066.A00LF
          vendor: Kingston
          physical id: 2
          serial: 19140606
          slot: ChannelB-DIMM0
          size: 8GiB
          width: 64 bits
          clock: 1400MHz (0.7ns)
     *-bank:3
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 3
          serial: [Empty]
          slot: ChannelB-DIMM1

quants discs i tipus es poden connectar
-Manual
- 2 x SATA 6Gb/s connectors (SATA3 0~SATA3 1) supporting up to 2 SATA 6Gb/s devices- 2 x SATA 3Gb/s connectors (SATA2 2~ SATA2 3) supporting up to 2 SATA 3Gb/s devices
chipset, enllaç a
-Enllaç pagina intel(chipset):https://ark.intel.com/content/www/es/es/ark/products/75016/intel-h81-chipset.html
-Enllaç diagrama de block: https://www.intel.la/content/www/xl/es/embedded/products/shark-bay/desktop/specifications.html
#cpu

model de CPU, any, nuclis, fils, memòria cau
-lscpu
      MODEL NAME: Intel(R) Pentium(R) CPU G3258 @ 3.20GHz
      ANY: 2014
      NUCLIS: 2
      Fils: 22nm
      L3 cache: 3 MiB




#pci

nombre de ranures PCI, carrils disponibles
-lspci
-PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #1 (rev d5)
-PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #3 (rev d5) 
-PCI bridge: Intel Corporation 82801 PCI Bridge (rev d5)

dispositius connectats
00:00.0 Host bridge: Intel Corporation 4th Gen Core Processor DRAM Controller (rev 06)
00:02.0 VGA compatible controller: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller (rev 06)
00:03.0 Audio device: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller (rev 06)
00:14.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB xHCI (rev 05)
00:16.0 Communication controller: Intel Corporation 8 Series/C220 Series Chipset Family MEI Controller #1 (rev 04)
00:1a.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #2 (rev 05)
00:1b.0 Audio device: Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller (rev 05)
00:1c.0 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #1 (rev d5)
00:1c.2 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #3 (rev d5)
00:1c.3 PCI bridge: Intel Corporation 82801 PCI Bridge (rev d5)
00:1d.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #1 (rev 05)
00:1f.0 ISA bridge: Intel Corporation H81 Express LPC Controller (rev 05)
00:1f.2 SATA controller: Intel Corporation 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode] (rev 05)
00:1f.3 SMBus: Intel Corporation 8 Series/C220 Series Chipset Family SMBus Controller (rev 05)
02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 06)
03:00.0 PCI bridge: Intel Corporation 82801 PCI Bridge (rev 41)

dispositivo de red, modelo, módulo de kernel, velocidad

dispositivo de audio, modelo, módulo de kernel
-lspci -v | grep "Audio" -A 12
       -  Audio device: Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller (rev 05)
        -Subsystem: Intel Corporation Device 2010
        -Kernel modules: snd_hda_intel

dispositivo vga, modelo, módulo de kernel
-lspci -v | grep "VGA" -A 12
-  VGA compatible controller: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller (rev 06) (prog-if 00 [VGA controller])
        -Gigabyte Technology Co., Ltd Device d000
        -Kernel modules: i915


#discos duros

/ dev / *, modelo, tipo de bus, velocidad del bus

prueba fio aleatoria (IOPS) y secuencial (MBps)


**18 de noviembre 2020**
#Extraure informacio del hardware maquina virtual
sistema

model de sistema, versió de bios i data de bios, quina edat té el maquinari?
-sudo dmidecode -s system-product-name
        -X10SRA-F
-sudo dmidecode -t bios 
        Vendor: American Megatrends Inc.
	Version: 2.0c
	Release Date: 09/25/2017
	Address: 0xF0000
	Runtime Size: 64 kB
	ROM Size: 16 MB
Aquest maquinari te 3 anys.

model de placa base, enllaç al manual, enllaç al producte
-sudo dmidecode -t baseboard 
        -Product Name: X10SRA-F
        -Enllaç manual: https://www.supermicro.com/manuals/motherboard/C612/MNL-1643.pdf
        -Enllaç producte: https://www.alternate.es/Supermicro/X10SRA-F-placa-base-para-servidor-y-estaci%C3%B3n-de-trabajo-LGA-2011-(Socket-R)-ATX-Intel-C612/html/product/1566461

bancs de memòria (lliures o ocupats)
-lshw -class memory

 *-bank:0
          description: DIMM DDR4 Synchronous 2400 MHz (0.4 ns)
          product: BLS8G4D240FSA.16FAD
          vendor: Undefined
          physical id: 0
          serial: A0195F1D
          slot: DIMMA1
          size: 8GiB
          width: 64 bits
          clock: 2400MHz (0.4ns)
     *-bank:1
          description: DIMM [empty]
          product: NO DIMM
          vendor: NO DIMM
          physical id: 1
          serial: NO DIMM
          slot: NO DIMM
     *-bank:2
          description: DIMM DDR4 Synchronous 2400 MHz (0.4 ns)
          product: BLS8G4D240FSA.16FAD
          vendor: Undefined
          physical id: 2
          serial: A61678A6
          slot: DIMMB1
          size: 8GiB
          width: 64 bits
          clock: 2400MHz (0.4ns)
     *-bank:3
          description: DIMM [empty]
          product: NO DIMM
          vendor: NO DIMM
          physical id: 3
          serial: NO DIMM
          slot: NO DIMM
     *-bank:4
          description: DIMM DDR4 Synchronous 2400 MHz (0.4 ns)
          product: BLS8G4D240FSA.16FAD
          vendor: Undefined
          physical id: 4
          serial: AD0528EC
          slot: DIMMC1
          size: 8GiB
          width: 64 bits
          clock: 2400MHz (0.4ns)
     *-bank:5
          description: DIMM [empty]
          product: NO DIMM
          vendor: NO DIMM
          physical id: 5
          serial: NO DIMM
          slot: NO DIMM
     *-bank:6
          description: DIMM DDR4 Synchronous 2400 MHz (0.4 ns)
          product: BLS8G4D240FSA.16FAR
          vendor: Undefined
          physical id: 6
          serial: 69200087
          slot: DIMMD1
          size: 8GiB
          width: 64 bits
          clock: 2400MHz (0.4ns)
     *-bank:7
          description: DIMM [empty]
          product: NO DIMM
          vendor: NO DIMM
          physical id: 7
          serial: NO DIMM
          slot: NO DIMM
 

quants discs i tipus es poden connectar

chipset, enllaç a

cpu

model de CPU, any, nuclis, fils, memòria cau

endoll

pci

nombre de ranures PCI, carrils disponibles

dispositius connectats

dispositiu de xarxa, model, mòdul del nucli, velocitat

dispositiu d'àudio, model, mòdul del nucli

dispositiu vga, model, mòdul del nucli

discs durs

/ dev / *, model, tipus de bus, velocitat del bus

prova fio random (IOPS) i seqüencial (MBps)

